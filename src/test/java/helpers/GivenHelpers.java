package helpers;

import ru.yandex.qatools.allure.annotations.Step;

import java.util.ArrayList;
import java.util.List;

import static com.codeborne.selenide.Selenide.*;
import static com.codeborne.selenide.WebDriverRunner.url;

public class GivenHelpers {

    @Step
    public static void given(Filter filter, Task ... tasks){
        ensureAppOpened();
        executeJavaScript(buildTaskScript(tasks));
        refresh();
        ensureAppOpened(filter);
    }

    @Step
    public static void given(TaskType taskType, String ... taskNames){
        given(Filter.ALL, aTasks(taskType, taskNames));
    }

    @Step
    public static void given(Task ... tasks){
        given(Filter.ALL, tasks);
    }

    @Step
    public static void givenAtActive(Task ... tasks){
        given(Filter.ACTIVE, tasks);
    }

    @Step
    public static void givenAtActive(TaskType taskType, String ... taskNames){
        givenAtActive(aTasks(taskType, taskNames));
    }

    @Step
    public static void givenAtCompleted(Task ... tasks){
        given(Filter.COMPLETED, tasks);
    }

    @Step
    public static void givenAtCompleted(TaskType taskType, String ... taskNames){
        givenAtCompleted(aTasks(taskType, taskNames));
    }

    @Step
    public static void ensureAppOpened(Filter filter){
        if(!(url().equals(filter))){
            open(filter.url());
        }
    }

    @Step
    public static void ensureAppOpened(){
        if(!(url().equals(Filter.ALL.url()))){
            open(Filter.ALL.url());
        }
    }

    public static Task aTask(TaskType taskType, String taskText){
        return new Task(taskText, taskType);
    }

    public static Task[] aTasks(TaskType taskType, String... taskNames){
        Task[] tasks = new Task[taskNames.length];
        for (int i = 0; i < taskNames.length; i++){
            tasks[i] = new Task(taskNames[i], taskType);
        }
        return tasks;
    }

    public static String buildTaskScript(Task ... tasks){
        List<String> taskTexts = new ArrayList<String>();

        for (Task task:tasks){
            taskTexts.add(task.toString());
        }
        return "localStorage.setItem('todos-troopjs', JSON.stringify([" + String.join(",", taskTexts) + "]))";
    }

    public enum TaskType {
        ACTIVE("'completed':false"),
        COMPLETED("'completed':true");

        String taskStatus;

        @Override
        public String toString() {
            return taskStatus;
        }

        TaskType(String taskStatus){
            this.taskStatus = taskStatus;
        }
    }

    public enum Filter{
        ALL(""),
        ACTIVE("/#/active"),
        COMPLETED("/#/completed");

        String subUrl;

        Filter(String subUrl){
            this.subUrl = subUrl;
        }

        public String url(){
            return "https://todomvc4tasj.herokuapp.com" + subUrl;
        }
    }

    public static class Task {
        private String taskText;
        private TaskType taskType;

        public Task(String taskText, TaskType taskType) {
            this.taskText = taskText;
            this.taskType = taskType;
        }

        @Override
        public String toString() {
            return "{" + taskType + ",'title':'" + taskText + "'}";
        }
    }
}
