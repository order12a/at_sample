import com.codeborne.selenide.ElementsCollection;
import com.codeborne.selenide.SelenideElement;
import org.junit.Test;
import org.openqa.selenium.By;
import ru.yandex.qatools.allure.annotations.Step;

import static com.codeborne.selenide.CollectionCondition.empty;
import static com.codeborne.selenide.CollectionCondition.exactTexts;
import static com.codeborne.selenide.Condition.*;
import static com.codeborne.selenide.Selenide.$;
import static com.codeborne.selenide.Selenide.$$;
import static helpers.GivenHelpers.*;
import static helpers.GivenHelpers.TaskType.ACTIVE;
import static helpers.GivenHelpers.TaskType.COMPLETED;

public class TodoMVCTest extends BaseTest{

    @Test
    public void testTasksManage() {
        given();

        create("t1");
        assertTasks("t1");

        //complete
        toggle("t1");
        assertTasks("t1");

        filterActive();
        assertNoVisibleTasks();

        create("t2");
        assertVisibleTasks("t2");

        //complete all
        toggleAll();
        assertNoVisibleTasks();

        filterCompleted();
        assertVisibleTasks("t1", "t2");

        //reopen
        toggle("t1");
        assertVisibleTasks("t2");

        clearCompleted();
        assertNoVisibleTasks();
        assertItemsLeft(1);

        filterAll();
        assertTasks("t1");
        assertItemsLeft(1);

        delete("t1");
        assertNoTasks();
    }

    @Test
    public void testCreateAtAll(){
        given(aTask(ACTIVE, "t1"));

        create("t2");
        assertTasks("t1", "t2");
        assertItemsLeft(2);
    }

    @Test
    public void testEditAtAll(){
        given(aTask(ACTIVE, "t1"));

        edit("t1", "t1 edited");
        assertTasks("t1 edited");
        assertItemsLeft(1);
    }

    @Test
    public void testEditAtActive(){
        givenAtActive(aTask(ACTIVE, "t1"), aTask(ACTIVE, "t2"));

        edit("t2", "t2 edited");
        assertVisibleTasks("t1", "t2 edited");
        assertItemsLeft(2);
    }


    @Test
    public void testDeleteAtAll(){
        given(aTask(ACTIVE, "t1"));

        delete("t1");
        assertNoTasks();
    }

    @Test
    public void testDeleteAtActive(){
        givenAtActive(ACTIVE, "t1", "t2");

        delete("t1");
        assertVisibleTasks("t2");
        assertItemsLeft(1);
    }

    @Test
    public void testDeleteAtCompleted(){
        givenAtCompleted(COMPLETED, "t1", "t2");

        delete("t2");
        assertVisibleTasks("t1");
        assertItemsLeft(0);
    }

    @Test
    public void testCompleteAtAll(){
        given(ACTIVE, "t1");

        toggle("t1");
        assertTasks("t1");
        assertItemsLeft(0);

        filterCompleted();
        assertVisibleTasks("t1");
    }

    @Test
    public void testCompleteAtActive(){
        givenAtActive(ACTIVE, "t1", "t2");

        toggle("t2");
        assertVisibleTasks("t1");
        assertItemsLeft(1);
    }

    @Test
    public void testCompleteAllAtAll(){
        given(ACTIVE, "t1", "t2", "t3");

        toggleAll();
        assertTasks("t1", "t2", "t3");
        assertItemsLeft(0);
    }

    @Test
    public void testReopenAllAtCompleted() {
        givenAtCompleted(COMPLETED, "t1", "t2", "t3");
        filterCompleted();

        toggleAll();
        assertNoVisibleTasks();
        assertItemsLeft(3);
    }

    @Test
    public void testReopenAtAll(){
        given(aTask(COMPLETED, "t1"));

        toggle("t1");
        assertTasks("t1");
        assertItemsLeft(1);
    }

    @Test
    public void testClearCompletedAtAll(){
        givenAtCompleted(aTask(COMPLETED, "t1"));

        clearCompleted();
        assertNoTasks();
    }

    @Test
    public void testSwitchFilterAllToCompleted(){
        given(aTask(ACTIVE, "t1"), aTask(COMPLETED, "t2"));

        filterCompleted();
        assertVisibleTasks("t2");
        assertItemsLeft(1);
    }

    @Test
    public void testSwitchFilterCompletedToActive(){
        givenAtCompleted(aTask(ACTIVE, "t1"), aTask(COMPLETED, "t2"));

        filterActive();
        assertVisibleTasks("t1");
        assertItemsLeft(1);
    }

    @Test
    public void testSwitchFilterActiveToAll(){
        given(aTask(ACTIVE, "t1"), aTask(COMPLETED, "t2"));

        filterAll();
        assertTasks("t1", "t2");
        assertItemsLeft(1);
    }

    @Test
    public void testCancelEditAtAll() {
        given(ACTIVE, "t1");

        cancelEdit("t1", "t1 edited");
        assertTasks("t1");
        assertItemsLeft(1);
    }

    @Test
    public void testCancelEditAtActive() {
        givenAtActive(ACTIVE, "t1");

        cancelEdit("t1", "t1 edited");
        assertVisibleTasks("t1");
    }

    @Test
    public void testConfirmEditByClickOutsideAtAll(){
        given(aTasks(ACTIVE, "t1"));

        confirmEditByClickingOutside("t1", "t1 edited");
        assertTasks("t1 edited");
        assertItemsLeft(1);
    }

    @Test
    public void testConfirmEditByPressTabAtActive(){
        givenAtActive(ACTIVE, "t1");

        confirmEditByPressTab("t1", "t1 edited");
        assertVisibleTasks("t1 edited");
        assertItemsLeft(1);
    }

    @Test
    public void testDeleteByEmptyingTextAtActive(){
        givenAtActive(ACTIVE, "t1", "t2");

        edit("t1", "");
        assertVisibleTasks("t2");
        assertItemsLeft(1);
    }

    ElementsCollection tasks = $$("#todo-list>li");

    @Step
    public void delete(String taskText) {
        tasks.find(exactText(taskText)).hover().find(".destroy").click();
    }

    @Step
    public void toggle(String taskText) {
        tasks.find(exactText(taskText)).find(".toggle").click();
    }

    @Step
    public void toggleAll() {
        $("#toggle-all").click();
    }

    @Step
    public void clearCompleted() {
        $("#clear-completed").click();
    }

    @Step
    public void filterAll() {
        $(By.linkText("All")).click();
    }

    @Step
    public void filterActive() {
        $(By.linkText("Active")).click();
    }

    @Step
    public void filterCompleted() {
        $(By.linkText("Completed")).click();
    }

    @Step
    public void edit(String oldTaskText, String newTaskText) {
        startEditing(oldTaskText, newTaskText).pressEnter();
    }

    @Step
    public void cancelEdit(String oldTaskText, String newTaskText) {
        startEditing(oldTaskText, newTaskText).pressEscape();
    }

    @Step
    public void confirmEditByPressTab(String oldTaskText, String newTaskText){
        startEditing(oldTaskText, newTaskText).pressTab();
    }

    @Step
    public void confirmEditByClickingOutside(String oldTaskText, String newTaskText){
        startEditing(oldTaskText, newTaskText);
        $("#header>h1").click();
    }

    @Step
    public void create(String... taskTexts) {
        for (String taskText : taskTexts) {
            $("#new-todo").val(taskText).pressEnter();
        }
    }

    @Step
    public SelenideElement startEditing(String oldTaskText, String newTaskText) {
        tasks.find(exactText(oldTaskText)).doubleClick();
        return tasks.find(cssClass("editing")).find(".edit").val(newTaskText);
    }

    @Step
    public void assertItemsLeft(int count) {
        $("#todo-count strong").shouldHave(exactText(String.valueOf(count)));
    }

    @Step
    public void assertVisibleTasks(String... taskTexts) {
        tasks.filter(visible).shouldHave(exactTexts(taskTexts));
    }

    @Step
    public void assertTasks(String... taskTexts) {
        tasks.shouldHave(exactTexts(taskTexts));
    }

    @Step
    public void assertNoVisibleTasks() {
        tasks.filter(visible).shouldBe(empty);
    }

    @Step
    public void assertNoTasks() {
        tasks.shouldBe(empty);
    }
}

